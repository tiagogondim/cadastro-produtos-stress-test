require 'net/http'
require 'byebug'
require 'json'

threads = []

#insere 100 registros na base de dados
(0..100).each do |x|
    threads << Thread.new {
      url = URI("http://localhost:3000/produtos")
      http = Net::HTTP.new(url.host, url.port)

      request = Net::HTTP::Post.new(url)
      request["Content-Type"] = 'application/json'
      request["Cache-Control"] = 'no-cache'
      request.body = "{ \"produto\" :\t{\n\t\t\t  \"sku\" : \"sku#{x.to_s}\",\n\t\t\t  \"nome\" : \"Nome#{x.to_s}\",\n\t\t\t  \"descricao\" : \"descricao#{x.to_s}\",\n\t\t\t  \"quantidade\" : #{x.to_s},\n\t\t\t  \"preco\" : #{x.to_s},\n\t\t\t  \"ean\" : \"12345678#{x.to_s}\"\n\t\t\t}\n}"

      response = http.request(request)
  }
end

threads.each(&:join)

#solicita envio de 100 relatorios por email
(0..100).each do |x|
  threads << Thread.new {
    url= URI('http://localhost:3000/produtos/report/request?send_to_email=tiago.gondim@skyhub.com.br')
    Net::HTTP.get(url)
  }
end

threads.each(&:join)

#recupera a lista dos registros no cadastro de produtos
url = URI('http://localhost:3000/produtos.json')
prod_list = Net::HTTP.get(url)
prod_list_json = JSON.parse(prod_list)

#exibe todos os registros incluidos
(prod_list_json).each do |produto|
  threads << Thread.new {
    url= URI('http://localhost:3000/produtos/' + produto["id"]["$oid"])
    Net::HTTP.get(url)
  }
end

threads.each(&:join)

#altera os  produtos do cadastro de produtos
(prod_list_json).each do |produto|
  threads << Thread.new {
    url = URI("http://localhost:3000/produtos/" + produto["id"]["$oid"])

    http = Net::HTTP.new(url.host, url.port)

    request = Net::HTTP::Put.new(url)
    request["Content-Type"] = 'application/json'
    request["Cache-Control"] = 'no-cache'
    request.body = "{ \"produto\" :\t{\n\t\t\t  \"sku\" : \"sku#{produto["sku"]}\",\n\t\t\t  \"nome\" : \"Nome#{produto["nome"]}\",\n\t\t\t  \"descricao\" : \"descricao#{produto["descricao"]}\",\n\t\t\t  \"quantidade\" : #{produto["quantidade"]},\n\t\t\t  \"preco\" : #{produto["preco"]},\n\t\t\t  \"ean\" : \"12345678\"\n\t\t\t}\n}"

    response = http.request(request)
  }
end

threads.each(&:join)

#pesquisa 100 vezes usando elasticsearch
(0..100).each do |x|
  threads << Thread.new {
    url= URI('http://localhost:3000/produtos/?keywors=skusku' + (x % 10).to_s)
    Net::HTTP.get(url)
  }
end

threads.each(&:join)

#remove todos os registros incluidos
(prod_list_json).each do |produto|
  threads << Thread.new {
    url = URI("http://localhost:3000/produtos/" + produto["id"]["$oid"])
    http = Net::HTTP.new(url.host, url.port)

    request = Net::HTTP::Delete.new(url)
    request["Content-Type"] = 'application/json'
    request["Cache-Control"] = 'no-cache'

    response = http.request(request)
  }
end

threads.each(&:join)
